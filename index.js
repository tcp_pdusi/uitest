const puppeteer = require('puppeteer')
let isElementVisible = async (page, cssSelector) => {
    let visible = true;
    await page.waitForSelector(cssSelector, { visible: true, timeout: 5000 }).catch(() => {visible = false;}).catch(err => console.log(err));
    return visible;
};

let getMeasure = async (page) => {
    
    const mypageurl = await page.url()
    const mypagetitle = await page.title()
    console.log("mypageurl= "+mypageurl)
    console.log("mypagetitle= "+mypagetitle)
    expect (mypageurl).to.contain('place')
    expect (mypagetitle).to.contain('place')
    console.log( await page.evaluate(() => JSON.stringify(performance.getEntriesByType('measure'), null, '')));

} 
describe ('Test Start', () => {
    let browser, page, index, selector, method;
    let i, topNav, subCategorycount, productCount, itemColorIndex, itemSizeCount = 0;
    let addToBag, visible = false;

    before (async function() {
        browser = await puppeteer.launch({
            headless: false,
           // executablePath:'/home/ec2-user/pupeteertestingproject/node_modules/puppeteer/.local-chromium/linux-706915/chrome-linux/chrome',
            args: ['--no-sandbox', '--start-maximized', '--disable-setuid-sandbox'], 
            slowMo: 0,
            devtools: false,
            timeout:    10000,
        })
        page = await browser.newPage()
        await page.setViewport({ width: 1280, height: 800 }).catch(err => console.log(err));
        //await page.setViewport({width: 1920, height: 1080}).catch(err => console.log(err));
        //await page.setViewport({ width: 1600, height:2000 }).catch(err => console.log(err));
        
    })


    it ('Home Page', async() => {

                await page.goto("https://www.childrensplace.com/us/home", 2000, { waitUntil: 'networkidle2' }).catch(err => console.log(err))
                await page.waitForSelector('#drawer-wrapper > div > nav > ul > li:nth-child(2) > a > div > span.nav-bar-item-label')
                getMeasure(page);
     })
     

     after (async function()  {
        await browser.close()
        
    })

        
})